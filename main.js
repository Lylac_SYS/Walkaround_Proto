(function(){
  console.log(`Loaded Walkaround Prototype`);

  const load = (function () {
    function _load(tag) {
      return function (id, url) {
        return new Promise(function (resolve, reject) {
          let element = document.createElement(tag);
          let parent = 'body';
          let attr = 'src';

          id ? element.id = id : id = null;
          element.className = 'asset';
          element.onload = function () {
            resolve(url);
          };
          element.onerror = function () {
            reject(url);
          };

          switch (tag) {
            case 'img':
              element.crossOrigin = "anonymous"
            case 'script':
              element.async = true;
              break;
            case 'link':
              element.type = 'text/css';
              element.rel = 'stylesheet';
              attr = 'href';
              parent = 'head';
          }

          element[attr] = url;
          document[parent].appendChild(element);
        });
      };
    }

    return {
      css: _load('link'),
      js: _load('script'),
      img: _load('img')
    };
  })();

  (()=>{
    var stop_scene;
    function startScene(scene) {
      stop_scene = scene;
      scene.initialize();
      document.querySelector('#loader_img').remove();
      scene.start();
    }

    MSPFA.slide.push(p => {
      //removes assets before loading. incase a vastly different animation was still loaded.
      document.querySelectorAll('.asset').forEach(function (a) {
        a.remove();
      });
      if (stop_scene) { stop_scene.stop(); stop_scene = undefined; }
      if (p == 31) {
        (async () => {
          await load.img(
            '00031_spriteSheet',
            '../Assets/00031_atlas_.png'
          );
          await load.js(
            'sAnimLib',
            '../Libraries/sAnimLib.js'
          );
          await load.js(
            'indexes',
            '../indexes.js'
          );
        })().then(function () {
          var _00031 = new sAnimLib.scene(
            '_00031',
            'Canvas',
            _00031Index,
            650,
            450,
            24,
            false
          );
          _00031.setInit(function () {
            this.ctx.imageSmoothingEnabled = false;
            this.selectionIndex = [
              { i: 1, x: 103, y: 166, w: 5, h: 165, k: function () { location.replace(`https://www.homestuck.com/bard-quest`); } },                        // bard quest
              { i: 2, x: 113, y: 166, w: 5, h: 165, k: function () { location.replace(`https://mrcheeze.github.io/andrewhussie/comic.html?comic=ch`); } }, // the caper havers
              { i: 3, x: 138, y: 166, w: 5, h: 160, k: function () { location.replace(`https://www.homestuck.com/problem-sleuth/titlescreen`); } },        // problem sleuth
              { i: 4, x: 143, y: 161, w: 5, h: 165, k: function () { location.replace(`https://mspfa.com/?s=20710&p=1`); } },                              // and it dont stop
              { i: 5, x: 173, y: 156, w: 5, h: 165, k: function () { location.replace(`https://www.homestuck.com/jailbreak`); } },                         // jail break
              { i: 6, x: 198, y: 156, w: 5, h: 165 }, // ghost buster 2 mmorpg
              { i: 7, x: 208, y: 166, w: 5, h: 150 }, // little monsters
              { i: 8, x: 228, y: 151, w: 5, h: 165 }, // harry anderson
            ];
            this.selectRegion = new Path2D();
            this.selectRegion.rect(103, 166, 5, 165);
            this.selectRegion.rect(113, 166, 5, 165);
            this.selectRegion.rect(138, 166, 5, 160);
            this.selectRegion.rect(143, 161, 5, 165);
            this.selectRegion.rect(173, 156, 5, 165);
            this.selectRegion.rect(198, 156, 5, 165);
            this.selectRegion.rect(208, 166, 5, 150);
            this.selectRegion.rect(228, 151, 5, 165);
            this.hover = undefined;
            this.setLayer('poster',[300,300]);
            this.setEvents({
              mousemove: (event) => {
                var BB = this.ctx.canvas.getBoundingClientRect();
                var mX = event.clientX - Math.round(BB.left);
                var mY = event.clientY - Math.round(BB.top);
                // this.debugFrames.status.innerHTML = mX + 'x|' + mY + 'y';
                if (this.ctx.isPointInPath(this.selectRegion, mX, mY)) {
                  if (this.hover === undefined) {
                    this.selectionIndex.forEach(s => {
                      if (mX > s.x && mX < s.x + s.w && mY > s.y && mY < s.y + s.h) {
                        this.hover = s;
                        this._0031Sheet.draw(this.layers.poster, s.i, [0, 0]);
                        this.ctx.drawImage(this.layers.poster.canvas, 326, 102);
                      }
                    });
                  }
                  if (
                    this.hover !== undefined &&
                    !(
                      mX > this.hover.x &&
                      mX < this.hover.x + this.hover.w &&
                      mY > this.hover.y && mY < this.hover.y + this.hover.h
                    )
                  ) {
                    this.hover = undefined;
                    this.layers.poster.clearRect(0, 0, this.layers.poster.canvas.width, this.layers.poster.canvas.height);
                  }
                } else if (this.hover !== undefined) {
                  this.hover = undefined;
                  this.layers.poster.clearRect(0, 0, this.layers.poster.canvas.width, this.layers.poster.canvas.height);
                }
              },
              click: () => {
                if (this.hover !== undefined && this.hover.k !== undefined) {
                  this.hover.k();
                }
              }
            });
            this.setSpriteSheet(
              '_0031Sheet',
              document.getElementById('00031_spriteSheet'),
              [
                [[302, 604], [650, 450]], // 00 background
                [[302, 0], [300, 300]],   // 01 bard quest
                [[604, 0], [300, 300]],   // 02 the caper havers
                [[0, 0], [300, 300]],     // 03 problem sleuth
                [[302, 302], [300, 300]], // 04 and it dont stop
                [[0, 302], [300, 300]],   // 05 jail break
                [[0, 906], [300, 300]],   // 06 little monsters game
                [[0, 604], [300, 300]],   // 07 ghost buster 2 mmorpg
                [[604, 302], [300, 300]], // 08 harry anderson
              ]
            );
          });
          startScene(_00031);
        })
        .catch(function(e) {
          // debugger;
          console.log(`Failed to load assets for page: ${MSPFA.p}\n${e.stack}`);
        });
      }
      if (p == 65) {
        (async () => {
          await load.img(
            '00065_spriteSheet',
            // '../Assets/00065_atlas_.png'
            // 'https://cdn.glitch.com/46e480ef-8913-422b-91b8-fb93df7d7d7f%2F00065_atlas_.png?v=1623032636206'
            'https://file.garden/W8EpTxwKHD_9SFxb/MSPFA/Test/00065_atlas_.png'
            // Go to your desktop vsc chrome debugger console and get the test code you were working on dunkass. :howhigh:
          );
          await load.js(
            'sAnimLib',
            '../Libraries/sAnimLib.js'
          );
          await load.js(
            'indexes',
            '../indexes.js'
          );
        })().then(function () {
          var _00065 = new sAnimLib.scene(
            '_00065',
            'Canvas',
            _00065Index,
            650,
            650,
            15,
            true
          );
          _00065.setInit(function () {
            this.setSpriteSheet(
              '_00065Sheet',
              document.getElementById('00065_spriteSheet'),
              [
                [[0, 0], [1413, 1413]],    // 00 Terminal
                [[0, 1415], [650, 650]],   // 01 Transition
                [[652, 1415], [52, 191]],  // 02 SC
                [[704, 1415], [66, 103]],  // 03 Rose
                [[772, 1415], [118, 87]],  // 04 Tea Pot
                [[652, 1415], [650, 650]], // 05 Template
                [[0,0], [5,5]]             // 06 Test
              ]
            );
          });
          startScene(_00065);
        })
        .catch(function (e) {
          console.log(`Failed to load assets for page: ${MSPFA.p}\n${e.stack}`);
        });;
      };
    });
    MSPFA.slide[0](65);
    // document.querySelector('#loader_img').remove();
  })();
})();
