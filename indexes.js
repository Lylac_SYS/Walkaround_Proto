console.log(`loaded indexes`);
//SceneIndex -> Object -> Layer -> FrameBlocks
//FrameBlocks are the raw form of frames
var _00031Index = {
  name:"_00031Index",
  objects:{
    Obj01:{
      name:"object 1",
      layers:{
        L01:{
          name:"Layer 1",
          Z_Order: 1,
          frames:[
            [
              [13],
              `this.ctx.canvas.addEventListener(this.events.mousemove.name, this.events.mousemove);
              this.ctx.canvas.addEventListener(this.events.click.name, this.events.click);`
            ],
            [
              [44, 47],
              `this.ctx.globalAlpha = alpha;
              this.ctx.fillStyle = '#25ff4a';
              this.ctx.fillRect(103, 166, 5, 165);
              this.ctx.fillRect(138, 166, 5, 160);
              this.ctx.fillRect(173, 156, 5, 165);
              this.ctx.globalAlpha = 1;`,
              [
                ['alpha', sAnimLib.util.getLongAcrossFrames, [0, 1]]
              ]
            ],
            [
              [48],
              `this.ctx.fillStyle = '#25ff4a';
              this.ctx.fillRect(103, 166, 5, 165);
              this.ctx.fillRect(138, 166, 5, 160);
              this.ctx.fillRect(173, 156, 5, 165);`
            ],
            [
              [49, 53],
              `this.ctx.globalAlpha = alpha;
              this.ctx.fillStyle = '#25ff4a';
              this.ctx.fillRect(103, 166, 5, 165);
              this.ctx.fillRect(138, 166, 5, 160);
              this.ctx.fillRect(173, 156, 5, 165);
              this.ctx.globalAlpha = 1;`,
              [
                ['alpha', sAnimLib.util.getLongAcrossFrames, [1, 0]]
              ]
            ],
            [
              [53],
              `this.time.currentFrame = 14;`
            ]
          ]
        },
        L02:{
          name:"Layer 2",
          Z_Order: 0,
          frames:[
            [
              [0], //frameScope
              `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
              this._0031Sheet.draw(this.buf, 0, [0, 0]);` //Initial function string
            ],
            [
              [5, 13],
              `this.ctx.globalAlpha = alpha;
              this.ctx.drawImage(this.buf.canvas, 0, 0);
              this.ctx.globalAlpha = 1;`,
              [ //commands
                //string to replace with result | function | arguments
                ['alpha', sAnimLib.util.getLongAcrossFrames, [0, 1]]
              ]
            ],
            [
              [14, 53],
              `this.ctx.drawImage(this.buf.canvas, 0, 0);
              this.ctx.drawImage(this.layers.poster.canvas, 326, 102);`
            ]
          ]
        }
      }
    }
  }
};
var _00065Index = {
  name:"_00065Index",
  objects:{
    Obj01:{
      name:"object 1",
      layers:{
        L01:{
          name:"Gifs",
          Z_Order: 0,
          frames:[
            [
              [0],
              ``
            ]
          ]
        },
        L02:{
          name:"Static",
          Z_Order: 1,
          frames: [
            [
              [0],
              `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
              // this._00065Sheet.draw(this.ctx, 0, [0, 0], {scale: 0.46});
              // this._00065Sheet.draw(this.buf, 1, [0, 0]);
              this._00065Sheet.draw(this.ctx, 0, [0,0], {blur2: [[87,0], [200,200], [25,-25]]});
              debugger;
              // this._00065Sheet.draw(this.ctx, 1, [0,0], {blur: [25,25]});`
            ],
            [
              [0, 39],
              `this.ctx.drawImage(this.buf.canvas, 0, 0);`
            ],
            [
              [40, 44],
              `this._00065Sheet.draw(this.ctx, 1, onePos, {blur: [100, 0]});
              this._00065Sheet.draw(this.ctx, 0, twoPos, {blur: [100, 0], scale: 0.46});`,
              [
                ['onePos', sAnimLib.util.getPointsAcrossFrames, [[0, 0], [-650, 0]]],
                ['twoPos', sAnimLib.util.getPointsAcrossFrames, [[650, 0], [0, 0]]],
              ]
            ]
          ]
        },
        L03:{
          name:"Tea Pot",
          Z_Order: 2,
          frames:[
            [
              [0],
              ``
            ]
          ]
        },
        L04:{
          name:"Script",
          Z_Order: 3,
          frames:[
            [
              [0],
              ``
            ]
          ]
        }
      }
    }
  }
};