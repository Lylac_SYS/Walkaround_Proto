console.log(`loaded indexes.js`);
//SceneIndex -> Object -> Layer -> FrameBlocks
//FrameBlocks are the raw form of frames
var _00031Index = {
  _Obj01:{
    _1Z_L1:[
      [
        [13],
        `this.ctx.canvas.addEventListener(this.events.mousemove.name, this.events.mousemove);
        this.ctx.canvas.addEventListener(this.events.click.name, this.events.click);`
      ],
      [
        [44, 47],
        `this.ctx.globalAlpha = alpha;
        this.ctx.fillStyle = '#25ff4a';
        this.ctx.fillRect(103, 166, 5, 165);
        this.ctx.fillRect(138, 166, 5, 160);
        this.ctx.fillRect(173, 156, 5, 165);
        this.ctx.globalAlpha = 1;`,
        [
          ['alpha', sAnimLib.util.getLongAcrossFrames, [0, 1]]
        ]
      ],
      [
        [48],
        `this.ctx.fillStyle = '#25ff4a';
        this.ctx.fillRect(103, 166, 5, 165);
        this.ctx.fillRect(138, 166, 5, 160);
        this.ctx.fillRect(173, 156, 5, 165);`
      ],
      [
        [49, 53],
        `this.ctx.globalAlpha = alpha;
        this.ctx.fillStyle = '#25ff4a';
        this.ctx.fillRect(103, 166, 5, 165);
        this.ctx.fillRect(138, 166, 5, 160);
        this.ctx.fillRect(173, 156, 5, 165);
        this.ctx.globalAlpha = 1;`,
        [
          ['alpha', sAnimLib.util.getLongAcrossFrames, [1, 0]]
        ]
      ],
      [
        [53],
        `this.time.currentFrame = 14;`
      ]
    ],
    _0Z_L2:[
      [
        [0], //frameScope
        `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
        this._0031Sheet.draw(this.buf, 0, [0, 0]);` //Initial function string
      ],
      [
        [5, 13],
        `this.ctx.globalAlpha = alpha;
        this.ctx.drawImage(this.buf.canvas, 0, 0);
        this.ctx.globalAlpha = 1;`,
        [ //commands
          //string to replace with result | function | arguments
          ['alpha', sAnimLib.util.getLongAcrossFrames, [0, 1]]
        ]
      ],
      [
        [14, 53],
        `this.ctx.drawImage(this.buf.canvas, 0, 0);
        this.ctx.drawImage(this.layers.poster.canvas, 326, 102);`
      ],
      //Test frame, remove later
      [
        [54, 154],
        `this._0031Sheet.draw(this.ctx, 3, roxyPos, {scale: roxyScale});
        this._0031Sheet.draw(this.ctx, 5, doorPos, {scale: doorScale});`,
        [
          ['roxyPos', sAnimLib.util.getPointsAcrossFrames, [[-77, 309], [66, 247]]],
          ['roxyScale', sAnimLib.util.getLongAcrossFrames, [2, 1.3]],
          ['doorPos', sAnimLib.util.getPointsAcrossFrames, [[315, 190], [309, 179]]],
          ['doorScale', sAnimLib.util.getLongAcrossFrames, [0.0515, 0.12]],
        ]
      ]
    ]
  }
};