(function() {
  console.log('loaded dev sAnimLib');
  /*
    TODO:
      Setup "Ease in and out" for vector like functions.
      Reverse Time? ~ cannot reverse time with how the timeline class is setup now.
      Reversing time can only be applied to static animations.
      Fuck it, add reverse time anyways.
      Add Sound utils.
      Speed up and slow down time.
      Redo how spritesheet class draws sprites. pre-render sprites instead. maybe draw them to their own canvases?

    Far down the line:
      No?~~Redo constructIndex to flatten each frame's function into an image instead. This might cause "not enough memory" errors.
        Also move to an Object focused timeline instead of a frame focused one. Each object within a scene having their own frameIndex
          function objects within an array within frames and frame blocks. [21,[func{},func{}]]
      Create "flash object within an object"
      Change draw method to rely on layers instead of spriteSheets (current scenario)
        this.spriteSheet.draw(); -> this.layer.draw();
  */
  window.sAnimLib = {
    //sections off an image for easy references by an index number.
    spriteSheet: class {
      constructor(sourceImage, arrayProperties) {
        this.sourceImage = sourceImage;
        this.arrayProperties = arrayProperties;
        this.one = sAnimLib.util.createCanvas();
        this.two = sAnimLib.util.createCanvas();
      }

      //Draws the image based on arguments inputted.
      //[Canvas Context, spriteSheet index number, [coordinates], options:{scale, rotate}]
      draw(ctx, index, xy, options = {}) {
        var pos, dim;
        pos = this.arrayProperties[index][0];
        dim = this.arrayProperties[index][1];
        this.one.canvas.width = this.two.canvas.width = dim[0];
        this.one.canvas.height = this.two.canvas.height = dim[1];

        //saves an image to the canvas to apply effects to
        this.one.drawImage(
          this.sourceImage,
          ...pos,
          ...dim,
          0,0,
          ...dim
        )

        if (options['blur']!=undefined){sAnimLib.effect.blur(this.one, options['blur']);}
        if (options['blur2']!=undefined){sAnimLib.effect.blur2(this.one, ...options['blur2']);}
        if (options['blur3']!=undefined){sAnimLib.effect.blur3(this.one, ...options['blur3']);}

        this.translate(this.two, xy, dim, options);

        // Manipulate a temporary canvas to keep the ctx to draw on clean.
        this.two.drawImage(
          this.one.canvas,
          0,0,
          ...dim,
          0,0,
          ...dim
        )

        ctx.drawImage(
          this.two.canvas,
          0,0,
          ctx.canvas.width,
          ctx.canvas.height,
          0,0,
          ctx.canvas.width,
          ctx.canvas.height
        );
      }
      translate(ctx, xy, dim, options = {}) {
        var scl, pol, wh, rot, origin;
        options[`scale`] ? scl = options[`scale`] : scl = [1,1];
        options[`rotate`] ? rot = options[`rotate`] : rot = [1,1];
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.setTransform(1,0,0,1,0,0);

        //drawImage returns an image from inside the canvas only.
        //determines how many params are in scl
        if (!Array.isArray(scl)) {
          Array.isArray(scl) ? (scl = scl[0]) : (scl = [scl, scl]);
        } else if (scl.length != 2) {
          scl = [scl[0], scl[0]];
        }
        //sets the polarity of either coordinate
        pol = [
          scl[0] < 0 ? -1 : 1,
          scl[1] < 0 ? -1 : 1
        ];
        //wh is used to offset the canvas when using a negative number
        wh = [
          pol[0] < 0 ? -1 * dim[0] : 0,
          pol[1] < 0 ? -1 * dim[1] : 0
        ];
        ctx.translate(xy[0],xy[1]);
        ctx.scale(...scl);
        ctx.translate(...wh);
        //determines which way to rotate the canvas.
        if (pol[0] < 0 ? pol[1] == 1 : pol[1] == -1) {
          rot = -1 * rot;
        }
        origin = [dim[0]/2, dim[1]/2];
        ctx.translate(origin[0], origin[1]);
        ctx.rotate(rot*Math.PI/180);
        ctx.translate(-origin[0], -origin[1]);
      }
    },
    //runs and sets everything for a scene object. can have multiple objects running at the same time.
    scene: class {
      constructor(name, canvas, index, width, height, fps, debug = false) {
        //scene vals
        this.name = name;
        window[name] = this;
        this.index = index;
        this.p = MSPFA.p;
        // this.timeline = new sAnimLib.timeline(fps, this, index);
        this.ctx = document.getElementById(canvas).getContext('2d');
        this.ctx.canvas.width = width; this.ctx.canvas.height = height;
        this.buf = sAnimLib.util.createCanvas(width, height);
        // this.timeline = new sAnimLib.timeline(fps, this, index);
        // this.timeline.context = document.getElementById(canvas);
        // this.timeline.buffer = document.createElement('canvas');
        // this.timeline.ctx = this.timeline.context.getContext('2d');
        // this.timeline.buf = this.timeline.buffer.getContext('2d');
        // this.timeline.ctx.canvas.width = this.timeline.buf.canvas.width = width;
        // this.timeline.ctx.canvas.height = this.timeline.buf.canvas.height = height;
        this.layers = [];
        this.canvas = [];
        this.map = new Map();
        this.key = ['Obj01'];
        this.time = {
          real: 0,
          ideal: 0,
          diff: 0,
          frameRate: 1000 / fps,
          counter: 1,
          currentFrame: 0,
          offset: 0,
          startTime: new Date().getTime(),
          breakLoop: false,
        };
        this.pause = undefined;
        this.setFrameIndex(index);
        //debugFrames
        if (debug) {
          this.debugFrames = document.createElement('div');
          this.debugFrames.style = 'text-align: left;';
          this.ctx.canvas.parentElement.parentElement.appendChild(this.debugFrames);
          this.debugFrames.appendChild(document.createElement('div'));
          this.debugFrames.lastChild.id = 'frames';
          this.debugFrames.frames = document.getElementById('frames');
          this.debugFrames.appendChild(document.createElement('div'));
          this.debugFrames.lastChild.id = 'status';
          this.debugFrames.status = document.getElementById('status');
          // this.time.g_currentFrame = 0;
        }
      }

      //Contains everything used by the scene
      setInit(func) {
        return this.initialize = func;
        // return this.initialize = func.bind(this.timeline);
      }

      //Sets custom methods to the scene
      setEvents(data) {
        return this.events = data;
      }

      //sets a spriteSheet to a scene object
      setSpriteSheet(name, sourceImage, arrayProperties) {
        return this[name] = new sAnimLib.spriteSheet(sourceImage, arrayProperties);
      }

      setLayer(name, size) {
        this.canvas[name] = document.createElement('canvas');
        this.layers[name] = this.canvas[name].getContext('2d');
        this.layers[name].canvas.width = size[0];
        this.layers[name].canvas.height = size[1];
      }

      delLayer(name) {
        this.layers[name].clearRect(0, 0, this.layers[name].canvas.width, this.layers[name].canvas.height);
        delete this.layers[name]; delete this.canvas[name];
      }

      setFrameIndex(index) {
        for (let objects in index.objects) {
          let tempIndex = {};
          tempIndex._currentFrame = 0;
          tempIndex.frames = sAnimLib.util.stripLayers(index.objects[objects]);
          tempIndex.frames = sAnimLib.util.constructFrames(tempIndex.frames);
          this.map.set(objects, tempIndex);
        }
      }

      getFrame(frame){
        let tempFrame;
        this.key.forEach((k)=>{
          if (this.map.get(k).frames[frame] !== undefined) {
            tempFrame = `//Index: ${this.index.name} || Object: ${k} || Frame: [${frame}]`;
            tempFrame = tempFrame + '\n' + this.map.get(k).frames[frame];
            try {
              tempFrame = Function(tempFrame);
            } catch(e) {
              console.error(e);
              console.error(`Current Frame:\n${frame}`);
            }
          }
        });
        return tempFrame;
      }

      //updates the time loop in a scene.
      //very accurate.
      //NEVER use Object.assign as it had caused MAJOR lag in the engine.
      updateTime() {
        if (this.time.ideal > 16000) {
          this.time.counter = 0;
          this.time.startTime = new Date().getTime();
        }
        // if (this.time.g_currentFrame !== undefined) { this.time.g_currentFrame = this.time.g_currentFrame + 1; }
        this.time.real = this.time.counter * this.time.frameRate;
        this.time.ideal = new Date().getTime() - this.time.startTime;
        this.time.diff = this.time.ideal - this.time.real;
        this.time.offset = this.time.frameRate - this.time.diff;
        this.time.currentFrame = this.time.currentFrame + 1;
        this.time.counter = this.time.counter + 1;
      }

      //Pauses a scene object. pause once to pause, then pause again to resume
      pauseTime() {
        if (this.pause) {
          this.time = this.pause;
          this.pause = undefined;
          console.log(`Unpaused Scene Object: ${this.name}`);
          this.time.breakLoop = false;
          requestAnimationFrame(() => this.start());
        } else {
          this.pause = this.time;
          console.log(`Paused Scene Object: ${this.name}`);
          this.time.breakLoop = true;
        }
      }

      resetTime() {
        this.time.real = 0;
        this.time.ideal = 0;
        this.time.diff = 0;
        this.time.counter = 1;
        this.time.currentFrame = 0;
        this.time.offset = 0;
        this.time.startTime = new Date().getTime();
      }

      sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
      }

      //This is what acctually starts playing and loops a js animation.
      start() {
        if (this.time.breakLoop == true) { return; }
        if (this.p !== MSPFA.p) { this.stop(); }
        let frame = this.getFrame(this.time.currentFrame);
        if (document.hidden === false) {
          this.ctx.clearRect(0,0, this.ctx.canvas.width, this.ctx.canvas.height);
          try {
            if (frame !== undefined) {
              frame.bind(this)();
            }
            // if (this.scene.frameIndex[this.time.currentFrame] !== undefined) {
            //   this.scene.frameIndex[this.time.currentFrame].bind(this)();
            // }
            if (this.debugFrames) {
              // this.debugFrames.frames.innerText = `|real___:${this.time.real}\n|ideal__:${this.time.ideal}\n|diff___:${this.time.diff}\n|frame__:${this.time.currentFrame}\n|g_frame:${this.time.g_currentFrame}\n|offset_:${this.time.offset}`;
              this.debugFrames.frames.innerText = `|real___:${this.time.real}\n|ideal__:${this.time.ideal}\n|diff___:${this.time.diff}\n|frame__:${this.time.currentFrame}\n|offset_:${this.time.offset}`;
              if (this.time.currentFrame > -1) {
                // this.time.breakLoop = true;
                // console.log((frame !== undefined)? frame.toString() : undefined);
                // debugger;
              }
            }
          } catch(e) {
            console.error(e);
            console.error(`Current Frame:\n${frame}`);
            this.time.breakLoop = true;
          }
          this.updateTime();
          setTimeout(() => this.start(), this.time.offset);
        } else if (document.hidden === true) {
          return requestAnimationFrame(() => this.start());
        }
      }

      //This stops the scene object from playing further and resets the canvas.
      stop() {
        this.ctx.globalCompositeOperation = 'source-over';
        this.buf.globalCompositeOperation = 'source-over';
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
        Object.entries(this.events).forEach((k) => {
          this.ctx.canvas.removeEventListener(k[0], k[1]);
        });
        this.resetTime();
        // if (this.time.g_currentFrame) { this.time.g_currentFrame = 0; }
        return console.log('stopped timer.');
      }
    },
    effect: {
      blur2: (ctx, [sourceX, sourceY], [width, height], [radiusX, radiusY]) => {
        if (isNaN(radiusX + radiusY)) return;
        radiusX, radiusY |= 0, 0;
        radiusX, radiusY = Math.abs(radiusX), Math.abs(radiusY);
        radiusX = Math.min(254, radiusX);
        radiusY = Math.min(254, radiusY);
        let radius = 25;

        let imgData = ctx.getImageData(sourceX, sourceY, width, height);
        let pixels = imgData.data;

        function BlurStack() {
          this.r = 0;
          this.g = 0;
          this.b = 0;
          this.a = 0;
          this.next = null;
        }

        let mul_table = [
          512, 512, 456, 512, 328, 456, 335, 512, 405, 328, 271, 456, 388, 335, 292, 512,
          454, 405, 364, 328, 298, 271, 496, 456, 420, 388, 360, 335, 312, 292, 273, 512,
          482, 454, 428, 405, 383, 364, 345, 328, 312, 298, 284, 271, 259, 496, 475, 456,
          437, 420, 404, 388, 374, 360, 347, 335, 323, 312, 302, 292, 282, 273, 265, 512,
          497, 482, 468, 454, 441, 428, 417, 405, 394, 383, 373, 364, 354, 345, 337, 328,
          320, 312, 305, 298, 291, 284, 278, 271, 265, 259, 507, 496, 485, 475, 465, 456,
          446, 437, 428, 420, 412, 404, 396, 388, 381, 374, 367, 360, 354, 347, 341, 335,
          329, 323, 318, 312, 307, 302, 297, 292, 287, 282, 278, 273, 269, 265, 261, 512,
          505, 497, 489, 482, 475, 468, 461, 454, 447, 441, 435, 428, 422, 417, 411, 405,
          399, 394, 389, 383, 378, 373, 368, 364, 359, 354, 350, 345, 341, 337, 332, 328,
          324, 320, 316, 312, 309, 305, 301, 298, 294, 291, 287, 284, 281, 278, 274, 271,
          268, 265, 262, 259, 257, 507, 501, 496, 491, 485, 480, 475, 470, 465, 460, 456,
          451, 446, 442, 437, 433, 428, 424, 420, 416, 412, 408, 404, 400, 396, 392, 388,
          385, 381, 377, 374, 370, 367, 363, 360, 357, 354, 350, 347, 344, 341, 338, 335,
          332, 329, 326, 323, 320, 318, 315, 312, 310, 307, 304, 302, 299, 297, 294, 292,
          289, 287, 285, 282, 280, 278, 275, 273, 271, 269, 267, 265, 263, 261, 259];
        let shg_table = [
          9, 11, 12, 13, 13, 14, 14, 15, 15, 15, 15, 16, 16, 16, 16, 17,
          17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 18, 19,
          19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20,
          20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 21,
          21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
          21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22,
          22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22,
          22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 23,
          23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
          23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
          23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
          23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
          24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
          24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
          24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
          24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24];

        let xxx, yyy, i, p;

        let yp;
        // ( yi ) pixel iterator
        let yw = yi = 0;
        // ( rbs ) random bull shit?
        let rbs;

        let pr, pg, pb, pa;

        let r_in_sum, g_in_sum, b_in_sum, a_in_sum;
        let r_sum, g_sum, b_sum, a_sum;
        let r_out_sum, g_out_sum, b_out_sum, a_out_sum;

        let div = radius + radius + 1;
        // let w4 = width << 2;
        let widthMinus1 = width - 1;
        let heightMinus1 = height - 1;
        let radiusPlus1 = radius + 1;
        let sumFactor = radiusPlus1 * (radiusPlus1 + 1) / 2;

        let stackStart = new BlurStack();
        let stack = stackStart;
        let stackIn = null;
        let stackOut = null;
        let stackEnd = null;

        let mul_sum = mul_table[radius];
        let shg_sum = shg_table[radius];

        for (i = 1; i < div; i++) {
          stack = stack.next = new BlurStack();
          if (i == radiusPlus1) stackEnd = stack;
        }
        stack.next = stackStart;

        for (yyy = 0; yyy < height; yyy++) {
          r_in_sum = g_in_sum = b_in_sum = a_in_sum = r_sum = g_sum = b_sum = a_sum = 0;

          pr = pixels[yi];
          pg = pixels[yi + 1];
          pb = pixels[yi + 2];
          pa = pixels[yi + 3];

          r_out_sum = radiusPlus1 * pr;
          g_out_sum = radiusPlus1 * pg;
          b_out_sum = radiusPlus1 * pb;
          a_out_sum = radiusPlus1 * pa;

          r_sum += sumFactor * pr;
          g_sum += sumFactor * pg;
          b_sum += sumFactor * pb;
          a_sum += sumFactor * pa;

          stack = stackStart;

          for (i = 0; i < radiusPlus1; i++) {
            stack.r = pr;
            stack.g = pg;
            stack.b = pb;
            stack.a = pa;
            stack = stack.next;
          }

          for (i = 1; i < radiusPlus1; i++) {
            p = yi + (Math.min(widthMinus1, i) << 2);
            rbs = radiusPlus1 - i;

            r_sum += (stack.r = (pr = pixels[p])) * rbs;
            g_sum += (stack.g = (pg = pixels[p + 1])) * rbs;
            b_sum += (stack.b = (pb = pixels[p + 2])) * rbs;
            a_sum += (stack.a = (pa = pixels[p + 3])) * rbs;

            r_in_sum += pr;
            g_in_sum += pg;
            b_in_sum += pb;
            a_in_sum += pa;

            stack = stack.next;
          }

          stackIn = stackStart;
          stackOut = stackEnd;

          for (xxx = 0; xxx < width; xxx++) {
            pixels[yi + 3] = pa = (a_sum * mul_sum) >> shg_sum;

            if (pa != 0) {
              pa = 255 / pa;
              pixels[yi] = ((r_sum * mul_sum) >> shg_sum) * pa;
              pixels[yi + 1] = ((g_sum * mul_sum) >> shg_sum) * pa;
              pixels[yi + 2] = ((b_sum * mul_sum) >> shg_sum) * pa;
            } else {
              pixels[yi] = pixels[yi + 1] = pixels[yi + 2] = 0;
            }

            r_sum -= r_out_sum;
            g_sum -= g_out_sum;
            b_sum -= b_out_sum;
            a_sum -= a_out_sum;

            r_out_sum -= stackIn.r;
            g_out_sum -= stackIn.g;
            b_out_sum -= stackIn.b;
            a_out_sum -= stackIn.a;

            p = xxx + radius + 1;
            p = (yw + Math.min(p, widthMinus1)) << 2;

            r_in_sum += (stackIn.r = pixels[p]);
            g_in_sum += (stackIn.g = pixels[p + 1]);
            b_in_sum += (stackIn.b = pixels[p + 2]);
            a_in_sum += (stackIn.a = pixels[p + 3]);

            r_sum += r_in_sum;
            g_sum += g_in_sum;
            b_sum += b_in_sum;
            a_sum += a_in_sum;

            stackIn = stackIn.next;

            r_out_sum += (pr = stackOut.r);
            g_out_sum += (pg = stackOut.g);
            b_out_sum += (pb = stackOut.b);
            a_out_sum += (pa = stackOut.a);

            r_in_sum -= pr;
            g_in_sum -= pg;
            b_in_sum -= pb;
            a_in_sum -= pa;

            stackOut = stackOut.next;

            yi += 4;
          }
          yw += width;
        }

        for (xxx = 0; xxx < width; xxx++) {
          r_in_sum = g_in_sum = b_in_sum = a_in_sum = r_sum = g_sum = b_sum = a_sum = 0;

          yi = xxx << 2;

          r_out_sum = radiusPlus1 * (pr = pixels[yi]);
          g_out_sum = radiusPlus1 * (pg = pixels[yi + 1]);
          b_out_sum = radiusPlus1 * (pb = pixels[yi + 2]);
          a_out_sum = radiusPlus1 * (pa = pixels[yi + 3]);

          r_sum += sumFactor * pr;
          g_sum += sumFactor * pg;
          b_sum += sumFactor * pb;
          a_sum += sumFactor * pa;

          stack = stackStart;

          for (i = 0; i < radiusPlus1; i++) {
            stack.r = pr;
            stack.g = pg;
            stack.b = pb;
            stack.a = pa;
            stack = stack.next;
          }

          yp = width;

          for (i = 1; i <= radius; i++) {
            yi = (yp + xxx) << 2;
            rbs = radiusPlus1 - i;

            r_sum += (stack.r = (pr = pixels[yi])) * rbs;
            g_sum += (stack.g = (pg = pixels[yi + 1])) * rbs;
            b_sum += (stack.b = (pb = pixels[yi + 2])) * rbs;
            a_sum += (stack.a = (pa = pixels[yi + 3])) * rbs;

            r_in_sum += pr;
            g_in_sum += pg;
            b_in_sum += pb;
            a_in_sum += pa;

            stack = stack.next;

            if (i < heightMinus1) {
              yp += width;
            }
          }

          yi = xxx;
          stackIn = stackStart;
          stackOut = stackEnd;

          for (yyy = 0; yyy < height; yyy++) {
            p = yi << 2;
            pixels[p + 3] = pa = (a_sum * mul_sum) >> shg_sum;

            if (pa > 0) {
              pa = 255 / pa;
              pixels[p] = ((r_sum * mul_sum) >> shg_sum) * pa;
              pixels[p + 1] = ((g_sum * mul_sum) >> shg_sum) * pa;
              pixels[p + 2] = ((b_sum * mul_sum) >> shg_sum) * pa;
            } else {
              pixels[p] = pixels[p + 1] = pixels[p + 2] = 0;
            }

            r_sum -= r_out_sum;
            g_sum -= g_out_sum;
            b_sum -= b_out_sum;
            a_sum -= a_out_sum;

            r_out_sum -= stackIn.r;
            g_out_sum -= stackIn.g;
            b_out_sum -= stackIn.b;
            a_out_sum -= stackIn.a;

            p = yyy + radiusPlus1;
            p = (xxx + (Math.min(p, heightMinus1) * width)) << 2;

            stackIn.r = pixels[p];
            stackIn.g = pixels[p + 1];
            stackIn.b = pixels[p + 2];
            stackIn.a = pixels[p + 3];

            r_in_sum += stackIn.r;
            g_in_sum += stackIn.g;
            b_in_sum += stackIn.b;
            a_in_sum += stackIn.a;

            r_sum += r_in_sum;
            g_sum += g_in_sum;
            b_sum += b_in_sum;
            a_sum += a_in_sum;

            stackIn = stackIn.next;

            pr = stackOut.r;
            pg = stackOut.g;
            pb = stackOut.b;
            pa = stackOut.a;

            r_out_sum += pr;
            g_out_sum += pg;
            b_out_sum += pb;
            a_out_sum += pa;

            r_in_sum -= pr;
            g_in_sum -= pg;
            b_in_sum -= pb;
            a_in_sum -= pa;

            stackOut = stackOut.next;

            yi += width;
          }
        }

        ctx.putImageData(imgData, sourceX, sourceY);

      },
      blur: (ctx, [x,y]) => {
        x = Math.round(x/2), y = Math.round(y/2);
        var width = ctx.canvas.width;
        var height = ctx.canvas.height;
        var aaa = sAnimLib.util.createCanvas(width,height);
        var bbb = sAnimLib.util.createCanvas(width,height);
        aaa.clearRect(0,0, aaa.canvas.width, aaa.canvas.height);
        bbb.clearRect(0,0, bbb.canvas.width, bbb.canvas.height);

        if (x!=0) {
          aaa.drawImage(ctx.canvas,0,0);
          aaa.globalAlpha = 0.1;
          for (var i=-x-1;i<0;i++) {
            aaa.drawImage(ctx.canvas,i,0);
            aaa.drawImage(ctx.canvas,-i,0);
          }
          bbb.drawImage(aaa.canvas,0,0);
          aaa.globalAlpha = 1;
          aaa.clearRect(0,0,width,height);
        }

        if ((x==0)&&(y!=0)) {
          aaa.drawImage(ctx.canvas,0,0);
          aaa.globalAlpha = 0.1;
          for (var j=-y-1;j<0;j++) {
            aaa.drawImage(ctx.canvas,0,j);
            aaa.drawImage(ctx.canvas,0,-j);
          }
          bbb.drawImage(aaa.canvas,0,0);
          aaa.globalAlpha = 1;
          aaa.clearRect(0,0,width,height);
        }

        if ((x!=0)&&(y!=0)) {
          aaa.drawImage(ctx.canvas,0,0);
          aaa.globalAlpha = 0.1;
          for (var j=-y-1;j<0;j++) {
            aaa.drawImage(bbb.canvas, 0,j);
            aaa.drawImage(bbb.canvas, 0,-j);
          }
          bbb.drawImage(aaa.canvas,0,0);
          aaa.globalAlpha = 1;
          aaa.clearRect(0,0,width,height);
        }

        ctx.clearRect(0,0,width,height);
        ctx.drawImage(bbb.canvas,0,0);
      }
    },
    //Utility Functions
    util: {
      //[77,113], [ [195, 185], [346, 155] ], 'this._yourSpriteSheet', [16,16,15,15]
      //Returns an array of alternating sprites. Useful for walk animations and the like.
      drawAcrossFrames: (frames, [points, sprites]) => {
        let temp = sAnimLib.util.getPointsAcrossFrames(frames, points);
        frames = frames[1] - frames[0];
        let output = [];
        for (let i = 0; i <= frames; i++) {
          output[i] = `${sprites[i % sprites.length]}, ${temp[i]}`;
        }
        return output;
      },
      //Returns an array of doubles to the 4th decimal place.
      getLongAcrossFrames: function(frames, scale) {
        let output = [];
        frames = frames[1] - frames[0];
        for (let i = 0; i <= frames; i++) {
          output[i] = Math.trunc((scale[0] + ((scale[1] - scale[0]) / frames) * i) * 10000) / 10000;
        }
        return output;
      },
      //Returns a pair of points given a percentage input. 0.5 = [5,1]
      getPointOnLineAtPercent: function(percent, points) {
        let output = [];
        function x(pnts) {
          output = [];
          if (pnts.length === 1) {
            output = [pnts[0][0], pnts[0][1]];
          } else {
            for (let i = 0; i < pnts.length - 1; i++) {
              output.push([
                Math.trunc(pnts[i][0] + (pnts[i + 1][0] - pnts[i][0]) * percent),
                Math.trunc(pnts[i][1] + (pnts[i + 1][1] - pnts[i][1]) * percent)
              ]);
            }
            x(output);
          }
        }
        x(points);
        return output;
      },
      //Returns an array of points. This plots a path from the given set of points and frames
      getPointsAcrossFrames: function(frames, points) {
        let output = [];
        frames = frames[1] - frames[0];
        for (let i = 0; i <= frames; i++) {
          output[i] = `[${sAnimLib.util.getPointOnLineAtPercent(Math.trunc((i / frames) * 100) / 100, points)}]`;
        }
        return output;
      },
      getRotateAcrossFrames: function(frames, degree) {
        let output = [];
        let temp = 0 - degree;
        frames = frames[1] - frames[0];
        for (let i = 0; i <= frames; i++) {
          output[i] = temp = temp + degree;
        }
        return output;
      },
      //Merges two indexes together.
      mergeIndex: function(frames, arrA, arrB) {
        //arrA: frameIndex, arrB: index To merge with frameIndex
        let as, bs;
        frames = [frames[0], frames[1] === undefined ? frames[0] : frames[1], frames[2] === undefined ? 1 : frames[2]];
        for (let i = frames[0]; i <= frames[1]; i += frames[2]) {
          if (arrB[i] !== undefined) {
            bs = arrB[i].toString();
            bs = bs.slice(bs.indexOf('{') + 2, bs.lastIndexOf('\n'));
            if (arrA[i] !== undefined) {
              as = arrA[i].toString();
              as = as.slice(as.indexOf('{') + 2, as.lastIndexOf('\n'));
              arrA[i] = new Function(as + '\n' + bs);
            } else {
              arrA[i] = new Function(bs);
            }
          }
        }
        return arrA;
      },
      //returns a simplified Array of the Object's frames
      stripLayers: function(object) {
        object = this.getLayerZOrder(object);
        let tempObject = [[]];
        for (let layers in object.layers) {
          let layer = object.layers[layers];
          tempObject[0] = tempObject[0].concat(layer.frames);
        }
        return tempObject[0];
      },
      //Sorts Layers based on their Z_Order property
      getLayerZOrder: function(object) {
        object.layers = Object.values(object.layers).sort((a,b) => a.Z_Order - b.Z_Order);
        return object;
      },
      //Calculates each frameBlock from an index array input. returns array.
      constructFrames: function(frameIndex){
        let tempIndex = [];
        // For each frameBlock in a frameIndex
        for (let frames in frameIndex) {
          let frame = frameIndex[frames];
          let funcStr = [];
          let commands = [];
          let frameScope = [frame[0][0], frame[0][1] === undefined ? frame[0][0] : frame[0][1], frame[0][2] === undefined ? 1 : frame[0][2]];
          if (frame[2] !== undefined) {
            // For each command, calculate and push result to array
            frame[2].forEach(function(a,b) {
              //console.log(`arguments:`, frameScope, ...a[2]);
              //Having trouble? Go to that function and set some of its args inside an array.
              commands[b] = a[1](frameScope, a[2]);
              //console.log('frame:', frame[2][b][0], ':', commands[b]);
            });
          }
          //Inside one frame at a time, Modify tempIndex from frameScope[0] to frameScope[1]
          for (let i = frameScope[0]; i <= frameScope[1]; i = i + frameScope[2]) {
            funcStr[i] = frame[1];
            if (frame[2] !== undefined) {
              //With each command in the block, edit funcStr
              commands.forEach(function(a, b) {
                funcStr[i] = funcStr[i].replace(frame[2][b][0], a[i - frameScope[0]]);
              });
            }
            if (tempIndex[i] !== undefined) {
              funcStr[i] = tempIndex[i] + '\n' + funcStr[i];
            }
            tempIndex[i] = funcStr[i];
          }
        }
        return tempIndex;
      },
      createCanvas: function(w=350,h=100) {
        var ctx = document.createElement('canvas').getContext('2d');
        ctx.canvas.width = w;
        ctx.canvas.height = h;
        return ctx;
      }
    }
  };
})();
