(()=>{
  var that = window._00065;
  var buf = that.buf;
  var ctx = that.ctx;
  var input = [that._00065Sheet,1]//4 params not 2, x & y
  var x = 25, y = 25;

  x = Math.round(x/2), y = Math.round(y/2);
  var temporary = document.createElement('canvas');
  var tmp = temporary.getContext('2d');
  tmp.canvas.width = that.ctx.canvas.width;
  tmp.canvas.height = that.ctx.canvas.height;

//   ctx.clearRect(0,0, ctx.canvas.width, ctx.canvas.height);
  buf.clearRect(0,0, buf.canvas.width, buf.canvas.height);
  tmp.clearRect(0,0, tmp.canvas.width, tmp.canvas.height);

//   ctx.fillStyle = 'white';
//   ctx.fillRect(0,0,650,650);

  if (x!=0) {
    buf.globalAlpha = 0.01;

    for (var i=-x-1;i<0;i++) {
      input[0].draw(buf, input[1], [i,0]);
      input[0].draw(buf, input[1], [-i,0]);
    }

    for (var k=0;k<10;k++) {
      tmp.drawImage(buf.canvas, 0,0);
    }

    buf.globalAlpha = 1;
    buf.clearRect(0,0, buf.canvas.width, buf.canvas.height);
  }

  if ((x==0)&&(y!=0)) {
    buf.globalAlpha = 0.01;

    for (var j=-y-1;j<0;j++) {
      input[0].draw(buf, input[1], [0,j]);
      input[0].draw(buf, input[1], [0,-j]);
    }

    for (var k=0;k<10;k++) {
      tmp.drawImage(buf.canvas, 0,0);
    }

    buf.globalAlpha = 1;
    buf.clearRect(0,0, buf.canvas.width, buf.canvas.height);
  }

  if ((x!=0)&&(y!=0)) {
    buf.globalAlpha = 0.1;

    for (var j=-y-1;j<0;j++) {
      buf.drawImage(tmp.canvas, 0,j);
      buf.drawImage(tmp.canvas, 0,-j);
    }

    // tmp.clearRect(0,0, tmp.canvas.width, tmp.canvas.height);

    for (var k=0;k<10;k++) {
      tmp.drawImage(buf.canvas, 0,0);
    }

    buf.globalAlpha = 1;
    buf.clearRect(0,0, buf.canvas.width, buf.canvas.height);
  }

  ctx.drawImage(tmp.canvas, 0,0);
})()