var _0031frameIndex = [
  [
    [0],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this._0031Sheet.draw(this.buf, 0, [0, 0]);`
  ],
  [
    [5, 13],
    `this.ctx.globalAlpha = alpha;
    this.ctx.drawImage(this.buf.canvas, 0, 0);
    this.ctx.globalAlpha = 1;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [0, 1]]
    ]
  ],
  [
    [13],
    `this.ctx.canvas.addEventListener(this.events.mousemove.name, this.events.mousemove);
    this.ctx.canvas.addEventListener(this.events.click.name, this.events.click);`
  ],
  [
    [14, 53],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);
    this.ctx.drawImage(this.layers.poster.canvas, 326, 102);`
  ],
  [
    [44, 47],
    `this.ctx.globalAlpha = alpha;
    this.ctx.fillStyle = '#25ff4a';
    this.ctx.fillRect(103, 166, 5, 165);
    this.ctx.fillRect(138, 166, 5, 160);
    this.ctx.fillRect(173, 156, 5, 165);
    this.ctx.globalAlpha = 1;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [0, 1]]
    ]
  ],
  [
    [48],
    `this.ctx.fillStyle = '#25ff4a';
    this.ctx.fillRect(103, 166, 5, 165);
    this.ctx.fillRect(138, 166, 5, 160);
    this.ctx.fillRect(173, 156, 5, 165);`
  ],
  [
    [49, 53],
    `this.ctx.globalAlpha = alpha;
    this.ctx.fillStyle = '#25ff4a';
    this.ctx.fillRect(103, 166, 5, 165);
    this.ctx.fillRect(138, 166, 5, 160);
    this.ctx.fillRect(173, 156, 5, 165);
    this.ctx.globalAlpha = 1;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [1, 0]]
    ]
  ],
  [
    [53],
    `this.time.currentFrame = 14;`
  ]
];
var _0090frameIndex = [
  
];
var _4118frameIndex = [
  [
    [0],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this.buf.fillStyle = 'black';
    this.buf.fillRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);`
  ],
  [
    [2, 20],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);`
  ],
  [
    [21],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this._4118Sheet.draw(this.buf, 1, [0, 0]);`
  ],
  [
    [21, 77],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);
    this._4118Sheet.draw(this.ctx, 4, bluh);`,
    [
      ['bluh', sAnimLib.util.getPointsAcrossFrames, [[-24, -121], [16, -42]]]
    ]
  ],
  [
    [77],
    `this.setFrameIndex(_4118SelectScreen);`
  ]
];
var _4158frameIndex = [
  [
    [0],
    `this.ctx.save();
    this.buf.save();
    this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this.buf.fillStyle = 'black';
    this.buf.fillRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);`
  ],
  [
    [2, 20],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);`
  ],
  [
    [21],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this._4118Sheet.draw(this.buf, 3, [0, 0]);`
  ],
  [
    [21, 77],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);
    this._4118Sheet.draw(this.ctx, 6, bluh);`,
    [
      ['bluh', sAnimLib.util.getPointsAcrossFrames, [[-180, 218], [-71, 202]]]
    ]
  ],
  [
    [77],
    `this.setFrameIndex(_4118SelectScreen);`
  ]
];
var _4190frameIndex = [
  [
    [0],
    `this.ctx.save();
    this.buf.save();
    this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this.buf.fillStyle = 'black';
    this.buf.fillRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);`
  ],
  [
    [2, 20],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);`
  ],
  [
    [21],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this._4118Sheet.draw(this.buf, 2, [0, 0]);`
  ],
  [
    [21, 77],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);
    this._4118Sheet.draw(this.ctx, 5, bluh);`,
    [
      ['bluh', sAnimLib.util.getPointsAcrossFrames, [[152, -213], [93, -106]]]
    ]
  ],
  [
    [77],
    `this.setFrameIndex(_4118SelectScreen);`
  ]
];
var _4118SelectScreen = [
  [
    [0],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);`
  ],
  [
    [0, 4, 2],
    `this.ctx.fillStyle = 'red';
    this.ctx.fillRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);`
  ],
  [
    [1, 5, 2],
    `this.ctx.fillStyle = 'yellow';
    this.ctx.fillRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);`
  ],
  [
    [6],
    `this.buf.fillStyle = 'black';
    this.buf.fillRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this.ctx.drawImage(this.buf.canvas, 0, 0, 650, 450);
    this._4118Sheet.draw(this.ctx, 0, [250, 173], {scale: 0.23});`
  ],
  [
    [7],
    `this.ctx.drawImage(this.buf.canvas, 0, 0, 650, 450);
    this._4118Sheet.draw(this.ctx, 0, [125, 87], {scale: 0.62});`
  ],
  [
    [8, 14, 2],
    `this._4118Sheet.draw(this.ctx, 10, [246, 5], {scale: 0.5});
    this.ctx.globalCompositeOperation = 'source-atop';
    this.ctx.fillStyle = 'red';
    this.ctx.fillRect(246, 5, 181, 9);`
  ],
  [
    [9, 15, 2],
    `this._4118Sheet.draw(this.ctx, 10, [246, 5], {scale: 0.5});
      this.ctx.globalCompositeOperation = 'source-atop';
      this.ctx.fillStyle = 'yellow';
      this.ctx.fillRect(246, 5, 181, 9);`
  ],
  [
    [36, 40, 2],
    `this._4118Sheet.draw(this.ctx, 10, [246, 5], {scale: 0.5});
      this.ctx.globalCompositeOperation = 'source-atop';
      this.ctx.fillStyle = 'red';
      this.ctx.fillRect(246, 5, 181, 9);`
  ],
  [
    [37, 39, 2],
    `this._4118Sheet.draw(this.ctx, 10, [246, 5], {scale: 0.5});
      this.ctx.globalCompositeOperation = 'source-atop';
      this.ctx.fillStyle = 'yellow';
      this.ctx.fillRect(246, 5, 181, 9);`
  ],
  [
    [8],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
      this.ctx.globalCompositeOperation = 'destination-over';
      this._4118Sheet.draw(this.ctx, 0, [0, 0]);
      this.ctx.globalCompositeOperation = 'source-over';
      this._4118Sheet.draw(this.buf, 7, [17, 78]);
      this.ctx.drawImage(this.buf.canvas, 0, 147, 650, 450);`
  ],
  [
    [9],
    `this.ctx.globalCompositeOperation = 'destination-over';
      this._4118Sheet.draw(this.ctx, 0, [0, 0]);
      this.ctx.globalCompositeOperation = 'source-over';
      this.ctx.drawImage(this.buf.canvas, 0, -5, 650, 450);`
  ],
  [
    [10],
    `this.ctx.globalCompositeOperation = 'destination-over';
      this._4118Sheet.draw(this.ctx, 0, [0, 0]);
      this.ctx.globalCompositeOperation = 'source-over';
      this.ctx.drawImage(this.buf.canvas, 0, 0, 650, 450);`
  ],
  [
    [10],
    `this.ctx.canvas.addEventListener(this.events.mousemove.name, this.events.mousemove);
    this.ctx.canvas.addEventListener(this.events.click.name, this.events.click);`
  ],
  //Default
  [
    [11],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
      this._4118Sheet.draw(this.buf, 0, [0, 0]);
      this._4118Sheet.draw(this.buf, 7, [17, 78]);`
  ],
  //Jane
  [
    [16, 20, 2],
    `this._4118Sheet.draw(this.ctx, 10, [246, 5], {scale: 0.5});
      this._4118Sheet.draw(this.ctx, 9, [309, 314]);
      this.ctx.globalCompositeOperation = 'source-atop';
      this.ctx.fillStyle = 'red';
      this.ctx.fillRect(246, 5, 342, 18);
      this.ctx.fillStyle = '#00D5F2';
      this.ctx.fillRect(309, 314, 27, 23);`
  ],
  [
    [17, 19, 2],
    `this._4118Sheet.draw(this.ctx, 10, [246, 5], {scale: 0.5});
      this._4118Sheet.draw(this.ctx, 9, [309, 315]);
      this.ctx.globalCompositeOperation = 'source-atop';
      this.ctx.fillStyle = 'yellow';
      this.ctx.fillRect(246, 5, 342, 18);
      this.ctx.fillStyle = '#00D5F2';
      this.ctx.fillRect(309, 315, 27, 23);`
  ],
  [
    [16],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
      this._4118Sheet.draw(this.buf, 0, [0, 0]);
      this._4118Sheet.draw(this.buf, 11, [8, 72]);
      this._4118Sheet.draw(this.buf, 7, [17, 78]);
      this._4118Sheet.draw(this.buf, 8, [17, 78]);`
  ],
  //Jake
  [
    [21, 25, 2],
    `this._4118Sheet.draw(this.ctx, 10, [246, 5], {scale: 0.5});
      this._4118Sheet.draw(this.ctx, 9, [309, 314]);
      this.ctx.globalCompositeOperation = 'source-atop';
      this.ctx.fillStyle = 'red';
      this.ctx.fillRect(246, 5, 342, 18);
      this.ctx.fillStyle = '#1F9400';
      this.ctx.fillRect(309, 314, 27, 23);`
  ],
  [
    [22, 24, 2],
    `this._4118Sheet.draw(this.ctx, 10, [246, 5], {scale: 0.5});
      this._4118Sheet.draw(this.ctx, 9, [309, 315]);
      this.ctx.globalCompositeOperation = 'source-atop';
      this.ctx.fillStyle = 'yellow';
      this.ctx.fillRect(246, 5, 342, 18);
      this.ctx.fillStyle = '#1F9400';
      this.ctx.fillRect(309, 315, 27, 23);`
  ],
  [
    [21],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
      this._4118Sheet.draw(this.buf, 0, [0, 0]);
      this._4118Sheet.draw(this.buf, 12, [8, 72]);
      this._4118Sheet.draw(this.buf, 7, [17, 78]);
      this._4118Sheet.draw(this.buf, 8, [172, 78]);`
  ],
  //Roxy
  [
    [26, 30, 2],
    `this._4118Sheet.draw(this.ctx, 10, [246, 5], {scale: 0.5});
      this._4118Sheet.draw(this.ctx, 9, [309, 314]);
      this.ctx.globalCompositeOperation = 'source-atop';
      this.ctx.fillStyle = 'red';
      this.ctx.fillRect(246, 5, 342, 18);
      this.ctx.fillStyle = '#F146EF';
      this.ctx.fillRect(309, 314, 27, 23);
      this.ctx.fillStyle = 'black';
      this.ctx.font = 'bold 13px Courier New';
      this.ctx.fillText('?', 319, 325);`
  ],
  [
    [27, 29, 2],
    `this._4118Sheet.draw(this.ctx, 10, [246, 5], {scale: 0.5});
      this._4118Sheet.draw(this.ctx, 9, [309, 315]);
      this.ctx.globalCompositeOperation = 'source-atop';
      this.ctx.fillStyle = 'yellow';
      this.ctx.fillRect(246, 5, 342, 18);
      this.ctx.fillStyle = '#F146EF';
      this.ctx.fillRect(309, 315, 27, 23);
      this.ctx.fillStyle = 'white';
      this.ctx.font = 'bold 13px Courier New';
      this.ctx.fillText('?', 319, 325);`
  ],
  [
    [26],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
      this._4118Sheet.draw(this.buf, 0, [0, 0]);
      this._4118Sheet.draw(this.buf, 13, [8, 72]);
      this._4118Sheet.draw(this.buf, 7, [17, 78]);
      this._4118Sheet.draw(this.buf, 8, [327, 78]);`
  ],
  //Dirk
  [
    [31, 35, 2],
    `this._4118Sheet.draw(this.ctx, 10, [246, 5], {scale: 0.5});
      this._4118Sheet.draw(this.ctx, 9, [309, 314]);
      this.ctx.globalCompositeOperation = 'source-atop';
      this.ctx.fillStyle = 'red';
      this.ctx.fillRect(246, 5, 342, 18);
      this.ctx.fillStyle = '#F2A400';
      this.ctx.fillRect(309, 314, 27, 23);
      this.ctx.fillStyle = 'white';
      this.ctx.font = 'bold 13px Courier New';
      this.ctx.fillText('?', 319, 325);`
  ],
  [
    [32, 34, 2],
    `this._4118Sheet.draw(this.ctx, 10, [246, 5], {scale: 0.5});
      this._4118Sheet.draw(this.ctx, 9, [309, 315]);
      this.ctx.globalCompositeOperation = 'source-atop';
      this.ctx.fillStyle = 'yellow';
      this.ctx.fillRect(246, 5, 342, 18);
      this.ctx.fillStyle = '#F2A400';
      this.ctx.fillRect(309, 315, 27, 23);
      this.ctx.fillStyle = 'black';
      this.ctx.font = 'bold 13px Courier New';
      this.ctx.fillText('?', 319, 325);`
  ],
  [
    [31],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
      this._4118Sheet.draw(this.buf, 0, [0, 0]);
      this._4118Sheet.draw(this.buf, 14, [8, 72]);
      this._4118Sheet.draw(this.buf, 7, [17, 78]);
      this._4118Sheet.draw(this.buf, 8, [482, 78]);`
  ],
  [
    [36],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
      this._4118Sheet.draw(this.buf, 0, [0, 0]);
      this._4118Sheet.draw(this.buf, 7, [17, 78]);`
  ],
  [
    [11, 40],
    `this.ctx.globalCompositeOperation = 'destination-over';
      this.ctx.drawImage(this.buf.canvas, 0, 0, 650, 450);
      this.ctx.globalCompositeOperation = 'source-over';`
  ],
  [
    [36],
    `this.hussieSheet.draw(this.ctx, 3, [223, 2], {scale: 0.09});
    this.lylacSheet.draw(this.ctx, 3, [427, 2], {scale: 0.09});`
  ],
  [
    [37],
    `this.hussieSheet.draw(this.ctx, 0, [223, 2], {scale: 0.09});
    this.lylacSheet.draw(this.ctx, 0, [427, 2], {scale: 0.09});`
  ],
  [
    [38],
    `this.hussieSheet.draw(this.ctx, 1, [223, 2], {scale: 0.09});
    this.lylacSheet.draw(this.ctx, 1, [427, 2], {scale: 0.09});`
  ],
  [
    [39],
    `this.hussieSheet.draw(this.ctx, 2, [223, 2], {scale: 0.09});
    this.lylacSheet.draw(this.ctx, 2, [427, 2], {scale: 0.09});`
  ],
  [
    [40],
    `this.hussieSheet.draw(this.ctx, 3, [223, 2], {scale: 0.09});
    this.lylacSheet.draw(this.ctx, 3, [427, 2], {scale: 0.09});`
  ],
  [
    [15],
    `this.time.currentFrame = 12;`
  ],
  [
    [20],
    `this.time.currentFrame = 17;`
  ],
  [
    [25],
    `this.time.currentFrame = 22;`
  ],
  [
    [30],
    `this.time.currentFrame = 27;`
  ],
  [
    [35],
    `this.time.currentFrame = 32;`
  ],
  [
    [40],
    `this.time.currentFrame = 37;`
  ],
];
var _4466frameIndex = [
  [
    [1],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this.buf.fillStyle = 'black';
    this.buf.fillRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);`
  ],
  [
    [1, 16],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);`
  ],
  [
    [17],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);`
  ],
  [
    [17, 47],
    `this._4466Sheet.draw(this.ctx, 1, pos, {scale: zoom});`,
    [
      ['zoom', sAnimLib.util.getLongAcrossFrames, [1.26, 1.70]],
      ['pos', sAnimLib.util.getPointsAcrossFrames, [[-82, -50], [-213, -131]]]
    ]
  ],
  [
    [48, 49],
    `this._4466Sheet.draw(this.ctx, 1, pos, {scale: zoom});`,
    [
      ['zoom', sAnimLib.util.getLongAcrossFrames, [7.2, 12.6]],
      ['pos', sAnimLib.util.getPointsAcrossFrames, [[-1931, -1182], [-3618, -2214]]]
    ]
  ],
  [
    [50],
    `this._4466Sheet.draw(this.buf, 2, [0, 0]);`
  ],
  [
    [50, 60],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);
    this.ctx.globalAlpha = alpha;
    this._4466Sheet.draw(this.ctx, 0, [248, 172], {scale: 0.23});
    this.ctx.globalAlpha = 1;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [0, 1]]
    ]
  ],
  [
    [60],
    `this.buf.drawImage(this.ctx.canvas, 0, 0);`
  ],
  [
    [61, 70],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);`
  ],
  [
    [71],
    `this.ctx.fillStyle = 'black';
    this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this._4466Sheet.draw(this.ctx, 0, [123, 86], {scale: 0.62});
    this.setFrameIndex(_4466SelectScreen);`
  ]
];
var _4513frameIndex = [
  [
    [1],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this.setLayer('FadeEffect',[650,450]);
    this.layers.FadeEffect.clearRect(0, 0, this.layers.FadeEffect.canvas.width, this.layers.FadeEffect.canvas.height);
    this.layers.FadeEffect.fillStyle = 'black';
    this.layers.FadeEffect.fillRect(0, 0, this.layers.FadeEffect.canvas.width, this.layers.FadeEffect.canvas.height);
    this.buf.drawImage(this.layers.FadeEffect.canvas, 0, 0);`
  ],
  //Background
  [
    [7],
    `this._4466Sheet.draw(this.buf, 3, [-6.5, -4.5], {scale: 1.02});
    this._4513Sheet.draw(this.buf, 10, [356, 182]);
    //this.debug3.draw(this.buf, 0, [193,182], {scale: [1,1], rotate: 15});`
  ],
  [
    [48],
    `this._4466Sheet.draw(this.buf, 3, [-6.5, -4.5], {scale: 1.02});`
  ],
  [
    [95],
    `this.buf.drawImage(this.layers.FadeEffect.canvas, 0, 0);`
  ],
  [
    [98],
    `this._4513Sheet.draw(this.buf, 15, [108, 196]);`
  ],
  [
    [104],
    `this.buf.drawImage(this.layers.FadeEffect.canvas, 0, 0);`
  ],
  [
    [1, 725],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);`
  ],
  //Foreground
  [
    [6, 27],
    `this.ctx.globalAlpha = alpha;
    this.ctx.drawImage(this.layers.FadeEffect.canvas, 0, 0);
    this.ctx.globalAlpha = 1;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [1, 0]]
    ]
  ],
  [
    [48, 55],
    `this._4513Sheet.draw(this.ctx, 12, pos, {rotate: deg});`,
    [
      ['pos', sAnimLib.util.getPointsAcrossFrames,
        [[354, 176], [341, 132], [353, 29], [291, -20], [303, 15], [332, -10], [274, 23], [274, 139]]
      ],
      ['deg', sAnimLib.util.getRotateAcrossFrames, -120]
    ]
  ],
  [
    [63, 95],
    `this.ctx.globalAlpha = alpha;
    this.ctx.drawImage(this.layers.FadeEffect.canvas, 0, 0);
    this.ctx.globalAlpha = 1;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [0, 1]]
    ]
  ],
  [
    [97, 102],
    `this.ctx.globalAlpha = alpha;
    this.ctx.drawImage(this.layers.FadeEffect.canvas, 0, 0);
    this.ctx.globalAlpha = 1;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [1, 0]]
    ]
  ],
  /* ========================================================== */
  [
    [104],
    `this._4513Sheet.draw(this.ctx, 15, [108, 201]);`
  ],
  [
    [105],
    `this.layers.FadeEffect.fillStyle = 'white';
    this.layers.FadeEffect.fillRect(0, 0, this.layers.FadeEffect.canvas.width, this.layers.FadeEffect.canvas.height);`
  ],
  [
    [106, 192],
    `this._4513Sheet.draw(this.ctx, bruh)`,
    [
      ['bruh', sAnimLib.util.drawAcrossFrames, [[[0,0]], [7,8,9,0,1,2,3,4,5,6]]]
    ]
  ],
  [
    [294, 603],
    `this._4513Sheet.draw(this.ctx, bruh)`,
    [
      ['bruh', sAnimLib.util.drawAcrossFrames, [[[0,0]], [7,8,9,0,1,2,3,4,5,6]]]
    ]
  ],
  [
    [105, 120],
    `this.ctx.globalAlpha = alpha;
    this.ctx.drawImage(this.layers.FadeEffect.canvas, 0, 0);
    this.ctx.globalAlpha = 1;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [1, 0]]
    ]
  ],
  [
    [106],
    `this._4513Sheet.draw(this.ctx, 15, [108, 153]);`
  ],
  [
    [107, 109],
    `this._4513Sheet.draw(this.ctx, 15, pos);`,
    [
      ['pos', sAnimLib.util.getPointsAcrossFrames, [[108, 115], [108, 73]]]
    ]
  ],
  [
    [110],
    `this._4513Sheet.draw(this.ctx, 15, [108, 8]);`
  ],
  [
    [105, 112],
    `this._4513Sheet.draw(this.ctx, 12, pos, {rotate: deg});`,
    [
      ['pos', sAnimLib.util.getPointsAcrossFrames,
        [[117, 187], [133, 279], [166, 286], [116.5, 239], [133, 249], [166, 174], [116.5, 127], [133, 137]]
      ],
      ['deg', sAnimLib.util.getRotateAcrossFrames, -120]
    ]
  ],
  [
    [113, 192],
    `this._4513Sheet.draw(this.ctx, 11, pos);`,
    [
      ['pos', sAnimLib.util.getPointsAcrossFrames, [[156, 66], [156, 151]]]
    ]
  ],
  [
    [193, 293],
    `this._4513Sheet.draw(this.ctx, 18, roxyPos, {scale: roxyScale});
    this._4513Sheet.draw(this.ctx, 20, doorPos, {scale: doorScale});`,
    [
      ['roxyPos', sAnimLib.util.getPointsAcrossFrames, [[-77, 309], [66, 247]]],
      ['roxyScale', sAnimLib.util.getLongAcrossFrames, [2, 1.3]],
      ['doorPos', sAnimLib.util.getPointsAcrossFrames, [[315, 190], [309, 179]]],
      ['doorScale', sAnimLib.util.getLongAcrossFrames, [0.0515, 0.12]],
    ]
  ],
  [
    [294, 440],
    `this._4513Sheet.draw(this.ctx, 13, roxyFour, {scale: [-0.5, 0.5]});
    this._4513Sheet.draw(this.ctx, 19, roxyTen);`,
    [
      ['roxyTen', sAnimLib.util.getPointsAcrossFrames, [[196, -481], [196, 19]]],
      ['roxyFour', sAnimLib.util.getPointsAcrossFrames, [[100, 214], [100, 71]]],
    ]
  ],
  [
    [441, 555],
    `this._4513Sheet.draw(this.ctx, 16, roxySeven);
    this._4513Sheet.draw(this.ctx, 17, roxyEight);`,
    [
      ['roxySeven', sAnimLib.util.getPointsAcrossFrames, [[42, -62], [42, -451]]],
      ['roxyEight', sAnimLib.util.getPointsAcrossFrames, [[246, -152], [246, 95]]],
    ]
  ],
  [
    [449, 555],
    `this._4513Sheet.draw(this.ctx, 14, roxySix);`,
    [
      ['roxySix', sAnimLib.util.getPointsAcrossFrames, [[329, -59], [325, 146]]]
    ]
  ],
  [
    [556, 603],
    `this._4513Sheet.draw(this.ctx, 11, pos, {scale: 0.2});
    this._4513Sheet.draw(this.ctx, 13, roxyFour, {scale: [-0.2, 0.2]});`,
    [
      ['pos', sAnimLib.util.getPointsAcrossFrames, [[329, 285], [329, 334]]],
      ['roxyFour', sAnimLib.util.getPointsAcrossFrames, [[274, 105], [274, 56]]]
    ]
  ],
  [
    [604, 650],
    `this.ctx.globalAlpha = alpha;
    this._4513Sheet.draw(this.ctx, bruh);
    this._4513Sheet.draw(this.ctx, 11, pos, {scale: 0.2});
    this._4513Sheet.draw(this.ctx, 13, roxyFour, {scale: [-0.2, 0.2]});
    this.ctx.globalAlpha = 1;`,
    [
      ['bruh', sAnimLib.util.drawAcrossFrames, [[[0,0]], [7,8,9,0,1,2,3,4,5,6]]],
      ['pos', sAnimLib.util.getPointsAcrossFrames, [[329, 335], [329, 384]]],
      ['roxyFour', sAnimLib.util.getPointsAcrossFrames, [[274, 55], [274, 7]]],
      ['alpha', sAnimLib.util.getLongAcrossFrames, [1, 0]]
    ]
  ],
  [
    [604, 650],
    `this.ctx.globalAlpha = beta;`,
    [
      ['beta', sAnimLib.util.getLongAcrossFrames, [0, 1]]
    ]
  ],
  [
    [678, 679],
    `this.ctx.globalAlpha = 0.99;`,
  ],
  [
    [680, 682],
    `this.ctx.globalAlpha = 0.98;`,
  ],
  [
    [682, 686],
    `this.ctx.globalAlpha = 0.96;`,
  ],
  [
    [687, 688],
    `this.ctx.globalAlpha = 0.95;`,
  ],
  [
    [689, 725],
    `this.ctx.globalAlpha = alpha;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [0.94, 0.76]]
    ]
  ],
  [
    [678, 725],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);
    this.ctx.globalAlpha = 1;`,
  ],
  [
    [604,724],
    `this._4513Sheet.draw(this.ctx, 20, doorPos, {scale: doorScale});
    this._4513Sheet.draw(this.ctx, 18, roxyPos, {scale: roxyScale});`,
    [
      ['roxyPos', sAnimLib.util.getPointsAcrossFrames, [[68, 247], [167, 280]]],
      ['roxyScale', sAnimLib.util.getLongAcrossFrames, [1.29, 0.8]],
      ['doorPos', sAnimLib.util.getPointsAcrossFrames, [[308, 178], [195, -27]]],
      ['doorScale', sAnimLib.util.getLongAcrossFrames, [0.13, 1.5]]
    ]
  ],
  [
    [604, 650],
    `this.ctx.globalAlpha = 1;`
  ],
  [
    [725],
    `this._4513Sheet.draw(this.ctx, 20, [42, -509], {scale: 3.37});
    this._4513Sheet.draw(this.ctx, 18, [-238, 71], {scale: 2.92});`//roxy
  ],
  [
    [726],
    `this._4513Sheet.draw(this.ctx, 18, [-649, -139], {scale: 5.07});`
  ],
  [
    [728],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this.buf.fillStyle = 'black';
    this.buf.fillRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);`
  ],
  [
    [728, 733],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);`
  ],
  [
    [734, 739],
    `this._4466Sheet.draw(this.buf, 0, [0, 0]);
    this.ctx.drawImage(this.buf.canvas, 0, 0);
    this.ctx.globalAlpha = alpha;
    this.ctx.fillStyle = 'black';
    this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this.ctx.globalAlpha = 1;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [1, 0]]
    ]
  ],
  [
    [739],
    `this.setFrameIndex(_4466SelectScreen);`
  ],
];
var _4565frameIndex = [
  [
    [1],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this.buf.fillStyle = 'black';
    this.buf.fillRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);`
  ],
  [
    [1, 27],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);`
  ],
  [
    [7, 27],
    `this._4466Sheet.draw(this.ctx, 5, [0, 0]);
    this._4466Sheet.draw(this.ctx, 14, [196, 185]);
    this._4466Sheet.draw(this.ctx, 13, [0, 378]);
    this.ctx.globalAlpha = alpha;
    this.ctx.drawImage(this.buf.canvas, 0, 0);
    this.ctx.globalAlpha = 1;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [1, 0]]
    ]
  ],
  [
    [28],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this._4466Sheet.draw(this.buf, 5, [0, 0]);`
  ],
  [
    [28, 51],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);
    this._4466Sheet.draw(this.ctx, 14, [196, 185]);`
  ],
  [
    [52],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this._4466Sheet.draw(this.buf, 4, [0, 0]);`
  ],
  [
    [52, 116],
    `this.ctx.drawImage(this.buf.canvas, 0, 0);`
  ],
  [
    [52, 75],
    `this.ctx.globalAlpha = alpha;
    this._4466Sheet.draw(this.ctx, 5, [0, 0]);
    this.ctx.globalAlpha = 1;
    this._4466Sheet.draw(this.ctx, 14, [196, 185]);`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [1, 0.0416]]
    ]
  ],
  [
    [76],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this._4466Sheet.draw(this.buf, 4, [0, 0]);`
  ],
  [
    [76, 116],
    `this._4466Sheet.draw(this.ctx, bruh);`,
    [
      ['bruh', sAnimLib.util.drawAcrossFrames, [[[195, 185], [346, 155]],[16, 16, 15, 15]]]
    ]
  ],
  [
    [28, 116],
    `this._4466Sheet.draw(this.ctx, 13, [0, 378]);`
  ],
  [
    [87, 116],
    `this.ctx.globalAlpha = alpha;
    this.ctx.fillStyle = 'white';
    this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this.ctx.globalAlpha = 1;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [0, 1]]
    ]
  ],
  [
    [117, 118],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this.buf.fillStyle = 'black';
    this.buf.fillRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
    this.ctx.drawImage(this.buf.canvas, 0, 0);`
  ],
  [
    [119, 126],
    `this._4466Sheet.draw(this.buf, 0, [0, 0]);
    this.ctx.drawImage(this.buf.canvas, 0, 0);
    this.ctx.globalAlpha = alpha;
    this.ctx.fillStyle = 'black';
    this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this.ctx.globalAlpha = 1;`,
    [
      ['alpha', sAnimLib.util.getLongAcrossFrames, [1, 0]]
    ]
  ],
  [
    [126],
    `this.setFrameIndex(_4466SelectScreen);`
  ],
];
var _4466SelectScreen = [
  [
    [0],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
      this._4466Sheet.draw(this.ctx, 0, [0, 0]);`
  ],
  [
    [1, 33, 2],
    `this._4466Sheet.draw(this.ctx, 12, [246, 5], {scale: 0.5});
      this.ctx.globalCompositeOperation = 'source-atop';
      this.ctx.fillStyle = 'red';
      this.ctx.fillRect(246, 5, 181, 9);`
  ],
  [
    [2, 34, 2],
    `this._4466Sheet.draw(this.ctx, 12, [246, 5], {scale: 0.5});
        this.ctx.globalCompositeOperation = 'source-atop';
        this.ctx.fillStyle = 'yellow';
        this.ctx.fillRect(246, 5, 181, 9);`
  ],
  [
    [1],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
        this.ctx.globalCompositeOperation = 'destination-over';
        this._4466Sheet.draw(this.ctx, 0, [0, 0]);
        this.ctx.globalCompositeOperation = 'source-over';
        this._4466Sheet.draw(this.buf, 10, [17, 78]);
        this.ctx.drawImage(this.buf.canvas, 0, 300, 650, 450);`
  ],
  [
    [2],
    `this.ctx.globalCompositeOperation = 'destination-over';
        this._4466Sheet.draw(this.ctx, 0, [0, 0]);
        this.ctx.globalCompositeOperation = 'source-over';
        this.ctx.drawImage(this.buf.canvas, 0, 147, 650, 450);`
  ],
  [
    [3],
    `this.ctx.globalCompositeOperation = 'destination-over';
        this._4466Sheet.draw(this.ctx, 0, [0, 0]);
        this.ctx.globalCompositeOperation = 'source-over';
        this.ctx.drawImage(this.buf.canvas, 0, -5, 650, 450);`
  ],
  [
    [4],
    `this.ctx.globalCompositeOperation = 'destination-over';
        this._4466Sheet.draw(this.ctx, 0, [0, 0]);
        this.ctx.globalCompositeOperation = 'source-over';
        this.ctx.drawImage(this.buf.canvas, 0, 0, 650, 450);
        this.ctx.canvas.addEventListener(this.events.mousemove.name, this.events.mousemove);
        this.ctx.canvas.addEventListener(this.events.click.name, this.events.click);`
  ],
  //Default
  [
    [5],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
        this._4466Sheet.draw(this.buf, 0, [0, 0]);
        this._4466Sheet.draw(this.buf, 10, [17, 78]);`
  ],
  //Jane
  [
    [10],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
        this._4466Sheet.draw(this.buf, 0, [0, 0]);
        this._4466Sheet.draw(this.buf, 6, [8, 72]);
        this._4466Sheet.draw(this.buf, 10, [17, 78]);
        this._4466Sheet.draw(this.buf, 11, [17, 78]);`
  ],
  //Jake
  [
    [15],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
        this._4466Sheet.draw(this.buf, 0, [0, 0]);
        this._4466Sheet.draw(this.buf, 7, [8, 72]);
        this._4466Sheet.draw(this.buf, 10, [17, 78]);
        this._4466Sheet.draw(this.buf, 11, [172, 78]);`
  ],
  //Roxy
  [
    [20],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
        this._4466Sheet.draw(this.buf, 0, [0, 0]);
        this._4466Sheet.draw(this.buf, 8, [8, 72]);
        this._4466Sheet.draw(this.buf, 10, [17, 78]);
        this._4466Sheet.draw(this.buf, 11, [327, 78]);`
  ],
  //Dirk
  [
    [25],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
        this._4466Sheet.draw(this.buf, 0, [0, 0]);
        this._4466Sheet.draw(this.buf, 9, [8, 72]);
        this._4466Sheet.draw(this.buf, 10, [17, 78]);
        this._4466Sheet.draw(this.buf, 11, [482, 78]);`
  ],
  [
    [30],
    `this.buf.clearRect(0, 0, this.buf.canvas.width, this.buf.canvas.height);
        this._4466Sheet.draw(this.buf, 0, [0, 0]);
        this._4466Sheet.draw(this.buf, 10, [17, 78]);`
  ],
  [
    [5, 34],
    `this.ctx.globalCompositeOperation = 'destination-over';
        this.ctx.drawImage(this.buf.canvas, 0, 0, 650, 450);
        this.ctx.globalCompositeOperation = 'source-over';`
  ],
  [
    [30],
    `this.hussieSheet.draw(this.ctx, 3, [223, 2], {scale: 0.09});
        this.lylacSheet.draw(this.ctx, 3, [427, 2], {scale: 0.09});`
  ],
  [
    [31],
    `this.hussieSheet.draw(this.ctx, 0, [223, 2], {scale: 0.09});
        this.lylacSheet.draw(this.ctx, 0, [427, 2], {scale: 0.09});`
  ],
  [
    [32],
    `this.hussieSheet.draw(this.ctx, 1, [223, 2], {scale: 0.09});
        this.lylacSheet.draw(this.ctx, 1, [427, 2], {scale: 0.09});`
  ],
  [
    [33],
    `this.hussieSheet.draw(this.ctx, 2, [223, 2], {scale: 0.09});
        this.lylacSheet.draw(this.ctx, 2, [427, 2], {scale: 0.09});`
  ],
  [
    [34],
    `this.hussieSheet.draw(this.ctx, 3, [223, 2], {scale: 0.09});
        this.lylacSheet.draw(this.ctx, 3, [427, 2], {scale: 0.09});`
  ],
  [
    [9],
    `this.time.currentFrame = 5;`
  ],
  [
    [14],
    `this.time.currentFrame = 10;`
  ],
  [
    [19],
    `this.time.currentFrame = 15;`
  ],
  [
    [24],
    `this.time.currentFrame = 20;`
  ],
  [
    [29],
    `this.time.currentFrame = 25;`
  ],
  [
    [34],
    `this.time.currentFrame = 30;`
  ]
]